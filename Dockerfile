FROM ubuntu:20.04

# Dockerfile for generating PyQRS.apk's

ARG CYTHON_VERSION=0.29.19

RUN apt-get update -qq && apt -y install \
        autoconf \
        automake \
        build-essential \
        git \
        libffi-dev \
        libltdl-dev \
        libssl-dev \
        libtool \
        openjdk-13-jdk --no-install-recommends \
        python3-pip \
        unzip \
        zip \
        zlib1g-dev \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && python3 -m pip install --upgrade \
        pip \
        Cython==$CYTHON_VERSION \
        buildozer 

ADD . build/
WORKDIR build

