'''
This module contains, apart from a few auxiliary functions, two type of classes:
subclasses of Distribution_data and classes derived from Distribution.
A probability distribution has characteristics which are special to this
distribution, but there are also functions, data, properties that can be
inherited from other distributions.
Take for example the chi-square distribution with parameter ν: its distribution
name, parameter names, parameter values and other characteristics are specific
for this distribution, but its support, probability density function, mean and
other functions can be derived from a gamma distribution with parameters ν/2
and 1/2.
These two types of characteristics determine the two types of subclasses:
ChiSquare_data contains the data and functions that are specific to the chi-
square distribution, whereas ChiSquare_distribution takes care of the pdf, cdf
and other functions which in this case are derived from a gamma distribution.
'''

from abc import abstractmethod
#from math import *
from math import isclose, lgamma, inf, sqrt, exp, pi, tan, atan, erf, isinf, log, floor
from random import random
import calc


# Auxiliary functions
# ===================

eps = 1.0e-8

def isclosetoint(x) -> bool:
    ''' returns True if x is close to an integer value and False if not '''
    return isclose(x, round(x), abs_tol=eps)

def log_choose(n, m) -> float:
    ''' logarithm of binomial coefficient '''
    return lgamma(n + 1) - lgamma(m + 1) - lgamma(n - m + 1)


def incbeta(x, a, b) -> float:
    ''' incomplete beta function '''
    
    def betacf(x, a, b) -> float:
        '''
        continued fraction (26.5.8) from Abramowitz & Stegun; p.188 from
        Numerical Recipes in Pascal, Press, Flannery, Teukolsky, Vetterling
        '''
        am = 1
        bm = 1
        az = 1
        aold = 0
        qab = a + b  # these qa's will be used
        qap = a + 1  # in factors which occur
        qam = a - 1  # in the coefficients
        bz = 1 - qab * x / qap
        m = 1
#        while not isclose(az, aold, rel_tol = eps): # is this a correct replacement for the next line?
        while abs(az - aold) >= eps * abs(az):
            tem = 2 * m
            d = m * (b - m) * x / ((qam + tem) * (a + tem))
            ap = az + d * am
            bp = bz + d * bm
            d = -(a + m) * (qab + m) * x / ((a + tem) * (qap + tem))
            app = ap + d * az
            bpp = bp + d * bz
            aold = az
            am = ap / bpp
            bm = bp / bpp
            az = app / bpp
            bz = 1
            m += 1
        return az

    if x <= 0:
        return 0
    if x >= 1:
        return 1
    bt = exp(a * log(x) + b * log(1 - x) + lgamma(a + b) - lgamma(a) - lgamma(b))
    if x < (a + 1) / (a + b + 2):
        return bt * betacf(x, a, b) / a
    else:
        return 1.0 - bt * betacf(1 - x, b, a) / b


def incgamma(x, a) -> float:
    ''' incomplete gamma function '''

    def gser(x, a) -> float:
        ''' see Numerical Recipes In Pascal, p.181 '''
        if x == 0:
            return 0
        ap = a
        sum_ = 1 / a
        del_ = sum_
        n = 1
        while abs(del_) > abs(sum_) * eps and n < 100:
            ap = ap + 1
            del_ = del_ * x / ap
            sum_ = sum_ + del_
            n += 1
        return sum_ * exp(-x + a * log(x) - lgamma(a))

    def gcf(x, a) -> float:
        ''' see Numerical Recipes In Pascal, p.181 '''
        g = 0
        gold = 1
        a0 = 1
        a1 = x
        b0 = 0
        b1 = 1
        fac = 1
        n = 1 
        ana = n - a
        a0 = (a1 + a0 * ana) * fac
        b0 = (b1 + b0 * ana) * fac
        anf = n * fac
        a1 = x * a0 + anf * a1
        b1 = x * b0 + anf * b1
        if not isclose(a1, 0, abs_tol = eps):
            gold = g
            fac = 1 / a1
            g = b1 * fac
        n += 1
        while abs((g - gold) / g) > eps: # replaced by:
#        while not isclose(g, gold):
            ana = n - a
            a0 = (a1 + a0 * ana) * fac
            b0 = (
                b1 + b0 * ana) * fac
            anf = n * fac
            a1 = x * a0 + anf * a1
            b1 = x * b0 + anf * b1
            if not isclose(a1, 0, abs_tol=eps):
                gold = g
                fac = 1 / a1
                g = b1 * fac
            n += 1
        return exp(-x + a * log(x) - lgamma(a)) * g

    if x < a + 1:
        return gser(x, a)
    else:
        return 1 - gcf(x, a)


def numeval(text):
    '''
    evaluate a numerical expression given as a string.
    input: text: string
    return: the evaluated value (float) if text contains a numerical expression;
            else None
    '''
    if text == '': return None
    text = text.replace('²','**2')
    text = text.replace('**', '^')
    return calc.evaluate(text)


def brentq(f, a, b) -> float:
    '''
    Brent's method is a root finding algorithm.
    For details see: https://en.wikipedia.org/wiki/Brent's_method
    input: (a pointer to) a function f, and initial brackets a and b,
        between which a root has to be found.
    return: the root of the function
    '''
    fa, fb = f(a), f(b)
    assert (fa * fb < 0), 'Function should have opposite signs in endpoints'
    if abs(fa) < abs(fb):
        a, b = b, a        # swap a and b (and thus fa and fb)
        fa, fb = fb, fa
    d = c = a
    fc = f(c)
    mflag = True
    while fb != 0 and not isclose(b,a, abs_tol = eps):
        if fa != fc and fb != fc:             # inverse quadratic interpolation
            s =  a * fb * fc / ((fa - fb) * (fa - fc)) \
                + b * fa * fc / ((fb - fa) * (fb - fc)) \
                + c * fa * fb / ((fc - fa) * (fc - fb))
        else:
            s = b - fb * (b - a) / (fb - fa)  # secant method
            in_between = ((3 * a + b) / 4 <= s <= b) or (
                b <= s <= (3 * a + b) / 4)
        if not in_between \
            or (mflag and abs(s - b) >= abs(b - c) / 2) \
            or (not mflag and abs(s - b) >= abs(c - d) / 2) \
            or (mflag and isclose(b, c, abs_tol=eps)) \
            or (not mflag and isclose(c, d, abs_tol=eps)):
            s = (a + b) / 2                   # bisection method
            mflag = True
        else:
            mflag = False
        fs = f(s)
        d = c  # d is assigned for the first time here; it isn't used 
        c = b  #     during the first iteration because the mflag is set
        if fa * fs < 0:
            b = s
            fb = fs
        else:
            a = s
            fa = fs
        if abs(fa) < abs(fb):
            a, b = b, a
            fa, fb = fb, fa
    return s


def Wilcoxon_Mann_Whitney_pmf(M, N):
    '''
    Calculates all needed probabilities for the
    Wilcoxon rank-sum test as well as
    for the equivalent Mann-Whitney test.    
    '''
    MN = M * N
    minMN = min(M, N)
    maxMN = max(M, N)
    frqncy = [0 for i in range(MN + 1)]
    for i in range(maxMN + 1):
        frqncy[i] = 1
    work = [0 for i in range(2 + minMN + MN // 2)]
    work[0] = 0
    for i in range(2, minMN + 1):
        work[i - 1] = 0
        n1 = i * maxMN
        for j in range(1 + n1 // 2):
            total = frqncy[j] + work[j]
            frqncy[j] = total
            work[i + j] = total - frqncy[n1 - j]
            frqncy[n1 - j] = total
    total = sum(frqncy)
    return  [frqncy[i] / total for i in range(MN+1)]
                                 
         
# Distribution
# ============

class Distribution(object):

    '''
.   variable/functions that render information about a probability distribution:
    support: tuple or list of minimum and maximum x for which pdf > 0 or pmf > 0
    in_support (function): True if the argument x is in the support, else False
    show_support (function) returns a string that shows the support
    parvalues: list of parameter values; a value is 'None' if unknown
    pdf: probability density function at the point x for continuous distribution
    pmf: probability mass function at the point x for discrete distribution
    cdf: cumulative distribution function at the point x
    quantile: inverse cumulative distribution function at the point q
            (sometimes called percentage point function, ppf)
    median (function): median
    mean (function): expected value, if it exists
    var (function): variance, if it exists
    std (function): standard deviation, if it exists
    sample (function): returns a list of a (quasi-)random sample of size n    
    '''
    
    support = (0, inf)  # most frequent situation
    parvalues: list
    
    def __init__(self, val):
        ''' initializes the Distribution object '''
        super().__init__()
        self.parvalues = val
 
    @abstractmethod
    def in_support(self, x):
        ''' True if x is in the support, else False '''
        pass
        
    @abstractmethod   
    def pdf(self, x) -> float: 
        ''' probability density function (for continuous distributions) '''
        pass

    @abstractmethod   
    def pmf(self, x) -> float: 
        ''' probability mass function (for discrete distributions) '''
        pass

    def cdf(self, x) -> float:
        ''' the cumulative distribution function '''
        if x <= self.support[0]: return 0.0
        if x >= self.support[1]: return 1.0
        return 0.0  # added in order to avoid complaints from syntax checker
        
    @abstractmethod   
    def quantile(self, q) -> float:
        '''
        inverse cumulative distribution function or quantile function
        at the point q (also called percentage point function)
        '''
        pass

    @abstractmethod
    def mean(self) -> float:
        ''' mean (expected value) of a distribution '''
        pass
    
    @abstractmethod
    def var(self) -> float:
        ''' variance of a distribution '''
        pass
    
    def median(self) -> float:
        ''' median of a distribution '''
        return self.quantile(0.5)

    @abstractmethod
    def show_support(self) -> str:
        pass

    def std(self) -> float:
        ''' standard deviation of a distribution '''
        return sqrt(self.var())

    def sample(self, n):
        ''' returns a list of a (quasi-)random sample of size n '''
        return [self.quantile(random()) for i in range(n)]


class Distribution_data(object):
    '''
    Contains information about some of a distribution's characteristics,
        especially concerning its parameters.
    distname: a tuple of the distribution's name (a string)
        and (a pointer to) the related distribution class
    parnames (list of strings): the parameter name(s)
    parspecs (list of spec): each spec (dictionary) holds info about type and
        value range of one parameter
    parhints (list of strings): the restrictions to the parameter value(s)
    parvalues: list of parameter values; a value is 'None' if unknown
    npars (property): the number of parameters (usually 1, 2 or 3)
    unknown_parameters: list of parameter positions with (still) unknown value
    all_known: True if all parameter values are known, else False
    iscontinuous (property): True if the distribution is continuous, else False
    isdiscrete (property): True if the distribution is discrete, else False
    note (optional string): a note about the distribution
    website (string): a web page (usually on Wikipedia) about the distribution
    '''
    
    distname: tuple
    parnames: list
    parspecs: list
    note: str = ''
    website: str = ''
    rv: Distribution

    def __init__(self, *val):
        super().__init__()
        self.parvalues = list(val)
        self.rv = self.distname[1](self.parvalues) if self.all_known() else None

    @property
    def isdiscrete(self):
        ''' is the distribution characterized by a probability mass function? '''
        return issubclass(self.distname[1], Discrete_distribution)
    
    @property
    def iscontinuous(self):
        ''' is the distribution characterized by a probability density function? '''
        return issubclass(self.distname[1], Continuous_distribution)

    @property
    def npars(self):
        return len(self.parnames)
    
    @property
    def parvalues(self):
        return self.__parvalues

    @parvalues.setter
    def parvalues(self, *args):
        '''
        Before assigning parvalues to self.__parvalues, *args is checked.
        a. For different input data types, numerical values, if any, have to be
            extracted. In case of a failure, a list of None(s) is returned. ###!!!
        b. Do the numerical values satisfy given constraints? If they don't,
            None(s) are returned.
        The argument *pars may be:
        . empty. i.e. an empty tuple, an empty list or an empty string.
            In these cases a list of None(s) is returned
            Example: rv = Normal_distribution(). Result is [None, None]
        . a tuple or list of which the first item is a list of length npars:
            In this case the first item is extracted and returned
            Example: rv = Normal_distribution([0, 1]). Result is [0, 1]
        . a tuple of length npars. If a tuple item is of type int or float,
            its value is returned else None is returned.
            Example: rv = Normal_distribution(0, 1). Result is [0, 1]
            If a tuple item is of type str, it is evaluated and returned.
            Example: rv = Normal_distribution('0', '1'). Result is [0, 1]           
        '''
        
        def component_wise(vals):
            '''
            Input: vals is a tuple of list of npars items. The items may be of
                type float or int or non-empty string. Its contents are checked
                for being of the correct type and whether they satisfy given
                constraints (in self.parspecs).
            '''

            def constraint_violated(value, spec) -> bool:
                '''
                checks whether value satisfies spec (a dictionary).
                An example of spec: {'integer': True, 'lo': '{0}', 'ielo': '>'}
                '''
                def error(condition, i):
                    ''' Is condition true for parameter i?
                        Then an exception is raised '''
                    if condition: raise ValueError(i)
                    return condition
                    
                result = False
                if 'integer' in spec and spec['integer'] == True:
                    result = error(not isclosetoint(value), i)

                if 'lo' in spec:
                    if isinstance(spec['lo'], str):
                        spec['lo'] = self.parvalues[int(spec['lo'][1])]
                    if spec.get('ielo', '<') in {'<=', '>='} or self.isdiscrete:
                        result = error(value < spec['lo'], i)
                    else:
                        result = error(value <= spec['lo'], i)

                if 'hi' in spec:
                    if isinstance(spec['hi'], str):
                        spec['hi'] = self.parvalues[int(spec['hi'][1])]
                    if spec.get('iehi', '<') in {'<=', '>='} or self.isdiscrete:
                        result = error(value > spec['hi'], i)
                    else:
                        result = error(value >= spec['hi'], i)                        

                return result

            # component_wise
            for i in range(self.npars):
                try:
                    if isinstance(vals[i], (float,int)):
                        self.__parvalues[i] = vals[i]
                    elif isinstance(vals[i], str) and vals[i]:
                        self.__parvalues[i] = numeval(vals[i])
                    if self.parvalues[i] != None:
                        if constraint_violated(self.parvalues[i], self.parspecs[i]):
                            self.parvalues[i] = None
                except ValueError:
                    raise ValueError(i)

        # setter
        self.__parvalues = [None] * self.npars
        pars = args[0]
        if pars:    # if pars not empty
            if isinstance(pars, (tuple,list)) and isinstance(pars[0], (tuple,list)):
                val = pars[0]
                if len(val) == self.npars:
                    component_wise(val)
            elif isinstance(pars, (tuple,list)) and len(pars) == self.npars:
                component_wise(pars)

    def unknown_parameters(self):
        ''' returns a list of parameter indices with (yet) unknown value '''
        return [i for i in range(self.npars) if self.parvalues[i] == None]

    def all_known(self) -> bool:
        ''' True if all parameter values are known, else False '''
        return self.unknown_parameters() == []
    
    @property
    def parhints(self):
        
        def sethint(name, spec) -> str:
            '''
            construction of a hint (type and admissable values) of a parameter
            input: name (string): name of the parameter
            input: spec (dictionary), keys of a dictionary can be:
                 integer: True or (default) False
                 lo: lower bound of parameter value
                 hi: upper bound of parameter value
                 ielo: nature of inequality for lo ('<=' or '<')
                 iehi: nature of inequality for hi ('<=' or '<')
            returns: hint (string)
            Example for the parameter 'n' of a binomial distribution:
                name = 'n'
                spec = dict(lo=0, integer=True)
                hint = 'n integer; n > 0'
            Example for the parameter 'p' of a binomial distribution:
                name = 'p'
                spec = dict(lo=0, hi=1, ielo='<=', iehi='<=')
                hint = '0 ≤ p ≤ 1'
            More examples: see derived distributions' variable 'parspec' 
            '''
            
            hint = ''
            if spec.get('integer', False):
                hint = name + ' is integer'
                if len(spec) > 1:
                    hint = hint + '; '
            if 'lo' in spec and 'hi' in spec:
                hint = hint + str(spec['lo']) + spec.get('ielo', ' < ') + \
                    name + spec.get('iehi', ' < ') + str(spec['hi'])
            elif 'lo' in spec:
                hint = hint + name + spec.get('ielo', ' > ') + str(spec['lo'])
            elif 'hi' in spec:
                hint = hint + name + spec.get('iehi', ' < ') + str(spec['hi'])
            return hint

        hints = [sethint(self.parnames[i], self.parspecs[i]) for i in range(self.npars)]
        for i in range(self.npars):
            hints[i] = hints[i].replace('>=', ' ≥ ')
            hints[i] = hints[i].replace('<=', ' ≤ ')
            hints[i] = hints[i].format(*self.parnames) 
        return hints


# Continuous_distribution
# =======================

class Continuous_distribution(Distribution):
    ''' a class derived from Distribution but still rather general'''    
    
    def in_support(self, x):
        '''
        The support is the interval where the pdf is positive.
        If x is between the two support limits, the result is True, else False
        '''
        return self.support[0] <= x <= self.support[1]

    def pmf(self, x):
        ''' in a continuous distribution the probability mass function is 0 '''
        return 0.0

    def quantile(self, q):
        '''
        Percentage point function or quantile function
        or inverse cumulative distribution function.
        input: a value q between 0 and 1.
        output: the corresponding quantile.
        Basically the fast Newton-Raphson method is used. This is possible
        because both the cdf and its derivative, the pdf, are given.
        Only if a step in the newton-Raphson iteration would lead to an x
        outside the the interval [xlo, xhi], the bisection method is used,
        thus guaranteeing that the next x will be inside this interval
        '''
        xlo, xhi = float(self.support[0]), float(self.support[1])  # result is in interval [xlo, xhi]
        if not isinf(self.mean()): 
            x = self.mean()   # finite mean
        else:
            if not isinf(xlo): # lower bound xlo is finite
                x = xlo+1
            else: x = 0  # lower bound is -∞
        xold = x - 1
        while not isclose(x, xold, abs_tol=eps):
            G = self.cdf(x) - q  # continuous non-decreasing function
            pdfx = self.pdf(x)   # pdf is derivative of G
            xold = x
            if G <= 0:
                xlo = x  # new lower bound
            if G >= 0:
                xhi = x  # new upper bound
            if (xhi - xlo) * pdfx <= abs(2 * G):
                x = (xhi + xlo) / 2  # bisection
            else:
                x = x - G / pdfx  # Newton-Raphson
        return x

    
    def show_support(self) -> str:
        ''' show the interval where the pdf is positive '''
        return str(self.support)


class Discrete_distribution(Distribution):
    ''' a class derived from Distribution but still rather general'''

    def in_support(self, x):
        '''
        The support is the set of points where the pmf is positive.
        If x is integer and between the two support limits, the result is True,
        else False
        '''
        return isclosetoint(x) and self.support[0] <= round(x) <= self.support[1]

    def pdf(self, x):
        ''' in a discrete distribution the probability density function is 0 '''
        return 0.0

    def cdf(self, x):
        ''' returns the cumulative distribution function '''
        if self.support[0] <= x <= self.support[1]:
            a, b = int(self.support[0]), int(floor(x))
            return sum(self.pmf(i) for i in range(a, b + 1))
        else:
            return super().cdf(x)

    def quantile(self, q):
        '''
        Percentage point function or quantile function
        or inverse cumulative distribution function.
        input: a value q between 0 and 1.
        result: the smallest value x for which P(X ≤ x) ≥ q.
        '''
        som = 0.0
        x = self.support[0] - 1
        while som < q:
            x += 1
            som += self.pmf(x)
        q = som
        return x

    def show_support(self) -> str:
        '''
        result: a string describing the set of values x for which P(X = x) > 0
        '''
        width = self.support[1] - self.support[0]
        result = '{' + str(self.support[0]) 
        if width > 1:
            result = result + ', ' + str(self.support[0] + 1) 
            if width > 2:
                result = result + ', ' + str(self.support[0] + 2) 
                if width > 3:
                    result = result + ', ...'
        if self.support[1] == inf:
            result = result + '} '
        else:
            result = result + ', ' + str(self.support[1]) + '}'    
        return result


# Beta_distribution
# ======================

class Beta_distribution(Continuous_distribution):
    support = (0, 1)

    def in_support(self, x) -> bool:
        return 0 < x < 1

    def pdf(self, x) -> float:
        [alpha, beta] = self.parvalues
        constant = lgamma(alpha + beta) - lgamma(alpha) - lgamma(beta)
        if not self.in_support(x):
            return 0
        if (alpha == 1) and (x == 0):
            return beta
        if (beta == 1) and (x == 1):
            return alpha
        return exp(constant + (alpha - 1) * log(x) + (beta - 1) * log(1 - x))

    def cdf(self, x) -> float:
        [alpha, beta] = self.parvalues
        if self.in_support(x):
            return incbeta(x, alpha, beta)
        else: return super().cdf(x)

    def mean(self) -> float:
        [alpha, beta] = self.parvalues
        return alpha / (alpha + beta)

    def var(self) -> float:
        [alpha, beta] = self.parvalues
        return self.mean() * beta / (alpha + beta) / (alpha + beta + 1)

class Beta_data(Distribution_data):
    distname = ('beta', Beta_distribution)
    parnames = ['α', 'β']
    parspecs = [dict(lo=0), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Beta_distribution'


# Binomial_distribution
# =====================

class Binomial_distribution(Discrete_distribution):

    @property
    def support(self):
        n = self.parvalues[0]
        return [0, n]       

    def pmf(self, x):
        [n, p] = self.parvalues
        if not self.in_support(x):
            return 0
        if isclosetoint(p):
            if x == n * p:
                return 1
            else:
                return 0
        else:
            return exp(log_choose(n, x) + x * log(p) + (n - x) * log(1 - p))

    def cdf(self, x):
        [n, p] = self.parvalues
        if self.support[0] <= x < self.support[1]:
            x = round(x) if isclosetoint(x) else floor(x)
            return 1 - incbeta(p, x + 1, n - x)
        else: return super().cdf(x)

    def mean(self):
        [n, p] = self.parvalues
        return n * p

    def var(self):
        [n, p] = self.parvalues
        return n * p * (1 - p)

class Binomial_data(Distribution_data):
    distname = ('binomial', Binomial_distribution)
    parnames = ['n', 'p']
    parspecs = [dict(lo=0, integer=True), dict(lo=0, hi=1, ielo='<=', iehi='<=')]
    website = 'http://en.wikipedia.org/wiki/Binomial_distribution'


# Bernoulli_distribution
# ======================

class Bernoulli_distribution(Binomial_distribution): # These lines
                                  # make the Bernoulli_distribution inherit   
    def __init__(self, val):      # from the Binomial_distribution.
        p = val[0]                # Without inheritance you could use the
        super().__init__([1, p])  # following lines by uncommenting them.
                                  
# class Bernoulli_distribution(Discrete_distribution):                                  
#    support = (0, 1)
#        
#     def pmf(self, x):
#         [p] = self.parvalues
#         if not self.in_support(x):
#             return 0.0
#         if isclosetoint(p):
#             return 1 - abs(x - p)
#         return p**x * (1 - p)**(1 - x)
# 
#     def cdf(self, x):
#         if x < 0: return 0
#         elif x >=1: return 1
#         else:
#             [p] = self.parvalues
#             return 1-p
# 
#     def quantile(self, q) -> float:
#         [p] = self.parvalues
#         if q <= 1 - p:
#             return 0
#         else:
#             return 1
# 
#     def mean(self) -> float:
#         [p] = self.parvalues
#         return p
# 
#     def var(self) -> float:
#         [p] = self.parvalues
#         return p * (1 - p)

class Bernoulli_data(Distribution_data):
    distname = ('Bernoulli', Bernoulli_distribution)
    parnames = ['p']
    parspecs = [dict(lo=0, hi=1)]
    website = 'http://en.wikipedia.org/wiki/Bernoulli_distribution'


# Cauchy_distribution
# ===================      
        
class Cauchy_distribution(Continuous_distribution):
    support = (-inf, inf)

    def pdf(self, x):
        [x0, _gamma] = self.parvalues
        return _gamma / pi / ((_gamma)**2 + (x - x0)**2)

    def cdf(self, x):
        [x0, _gamma] = self.parvalues
        return 0.5 + atan((x - x0) / _gamma) / pi

    def quantile(self, q):
        [x0, _gamma] = self.parvalues
        return x0 + _gamma * tan(pi * (q - 0.5))

    def median(self):
        return self.parvalues[0] # = x0

    def mean(self):
        return inf

    def var(self):
        return inf

class Cauchy_data(Distribution_data):
    distname = ('Cauchy', Cauchy_distribution)
    parnames = ['x0', 'γ']  # or ['xₒ', 'γ']
    parspecs = [dict(), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Cauchy_distribution'


# Gamma_distribution
# ==================

class Gamma_distribution(Continuous_distribution):

    def __init__(self, val):
        (self.alpha, self.beta) = val
        if self.alpha != None and self.beta != None:
            super().__init__(val)


    def pdf(self, x):
        constant = exp((self.alpha)  * log(self.beta) - lgamma(self.alpha)) \
            if self.alpha>0 else 1
        if not self.in_support(x):
            return 0
        if self.in_support(x) and x > 0:
            return constant * exp((self.alpha - 1) * log(x) - self.beta * x)
        if self.alpha == 1 and x == 0:
            return self.beta
        return 0

    def cdf(self, x):
        if self.in_support(x):
            if self.alpha < 500:
                return incgamma(x * self.beta, self.alpha)
            return (1 + incgamma((pow(self.beta * x / self.alpha, 1 / 3) - 1 +
                        1 / (9 * self.alpha))**2 * 4.5 * self.alpha, 0.5)) / 2
        else: return super().cdf(x)

    def mean(self):
        return self.alpha / self.beta

    def var(self):
        return self.alpha / self.beta**2


class Gamma_scale_distribution(Gamma_distribution):

    def __init__(self, val):
        (self.k, self.theta) = val
        if self.k != None and self.theta != None:
            super().__init__((self.k, 1/self.theta))

class Gamma_data(Distribution_data):
    distname = ('gamma (α,β)', Gamma_distribution)
    parnames = ['α', 'β']
    parspecs = [dict(lo=0), dict(lo=0)]
    note = 'Parameters: shape α and rate β; E(X) = α/β'
    website = 'http://en.wikipedia.org/wiki/Gamma_distribution'

class Gamma_data_scale(Distribution_data):
    distname = ('gamma (k,θ)', Gamma_scale_distribution)
    parnames = ['k', 'θ']
    parspecs = [dict(lo=0), dict(lo=0)]
    note = 'Parameters: shape k and scale θ; E(X) = kθ'
    website = 'http://en.wikipedia.org/wiki/Gamma_distribution'


# ChiSquare_distribution
# ======================

class ChiSquare_distribution(Gamma_distribution):

    def __init__(self, val):
        nu = val[0]
        if nu != None:
            super().__init__([0.5 * nu, 0.5]) # Inherit from the gamma distribution

class ChiSquare_data(Distribution_data):
    distname = ('chi-square', ChiSquare_distribution)
    parnames = ['ν']
    parspecs = [dict(lo=0)]
#    note = 'the parameter is not restricted to integer values.'
    website = 'http://en.wikipedia.org/wiki/Chisquare_distribution'


# Exponential_distribution
# ========================

class Exponential_distribution(Gamma_distribution):

    def __init__(self, val):
        labda = val[0]
        if labda != None:
            super().__init__([1, labda]) # Inherit from the gamma distribution

class Exponential_theta_distribution(Gamma_distribution):

    def __init__(self, val):
        theta = val[0]
        if theta != None:
            super().__init__([1, 1/theta]) # Inherit from the gamma distribution

### Alternatively, without inheritance, uncomment the following lines instead
# class Exponential_distribution(Continuous_distribution):
# 
#     def pdf(self, x):
#         [labda] = self.parvalues
#         if not self.in_support(x): return 0
#         return labda * exp(-labda * x)
# 
#     def cdf(self, x):
#         [labda] = self.parvalues
#         if self.in_support(x):
#             return 1 - exp(-labda * x)
#         else: return super().cdf(x)
# 
#     def quantile(self, q):
#         [labda] = self.parvalues      
#         return -log(1 - q) / labda
# 
#     def mean(self):
#         [labda] = self.parvalues      
#         return 1 / labda
# 
#     def var(self):
#         [labda] = self.parvalues      
#         return 1 / labda**2

class Exponential_data(Distribution_data):
    distname = ('exponential (λ)', Exponential_distribution)
    parnames = ['λ']
    parspecs = [dict(lo=0)]
    note = 'Parameter λ = 1/E(X)'
    website = 'http://en.wikipedia.org/wiki/Exponential_distribution'

class Exponential_theta_data(Distribution_data):
    distname = ('exponential (θ)', Exponential_theta_distribution)
    parnames = ['θ']
    parspecs = [dict(lo=0)]
    note = 'Parameter θ = E(X)'
    website = 'http://en.wikipedia.org/wiki/Exponential_distribution'

# DiscreteUniform_distribution
# ============================
           
class DiscreteUniform_distribution(Discrete_distribution):
    
    @property
    def support(self):
        return self.parvalues
    
    def pmf(self, x):
        [a, b] = self.parvalues
        if not self.in_support(x): return 0.0
        return 1 / (b - a + 1)

    def cdf(self, x):
        [a, b] = self.parvalues
        if self.support[0] <= x < self.support[1]:
            return (floor(x) - a + 1) / (b - a + 1)
        else: return super().cdf(x)

    def mean(self): 
        [a, b] = self.parvalues
        return (a + b) / 2        

    def var(self): 
        [a, b] = self.parvalues
        return (b - a) * (b - a + 2) / 12

class DiscreteUniform_data(Distribution_data):
    distname = ('discrete-uniform', DiscreteUniform_distribution)
    parnames = ['a', 'b']
    parspecs = [dict(integer=True), dict(integer=True, lo='{0}', ielo='>')]
    website = 'http://en.wikipedia.org/wiki/Uniform_distribution_(discrete)'


# DoubleExponential_distribution
# ==============================

class DoubleExponential_distribution(Continuous_distribution):
    support = (-inf, inf)

    def pdf(self, x):
        [mu, b] = self.parvalues
        return exp(-abs(x - mu) / b) / b / 2

    def cdf(self, x):
        [mu, b] = self.parvalues
        if x <= mu:
            return exp((x - mu) / b) / 2
        return 1 - exp((mu - x) / b) / 2

    def mean(self):
        return self.parvalues[0] # = mu

    def var(self):
        return 2 * self.parvalues[1]**2 # = 2 * b**2

class DoubleExponential_data(Distribution_data):
    distname = ('double-exponential', DoubleExponential_distribution)
    parnames = ['μ', 'b']
    parspecs = [dict(), dict(lo=0)]
    note = 'called Laplace or Double Exponential distribution.'
    website = 'http://en.wikipedia.org/wiki/Laplace_distribution'


# F_distribution
# ==============

class F_distribution(Continuous_distribution):

    def __init__(self, val):
        super().__init__(val)
        self.parvalues = val
        [df1, df2] = self.parvalues
        self.cBeta = Beta_distribution([df1 / 2, df2 / 2])
        if df2 > 1000:
            self.cp = ChiSquare_distribution([df1])
        elif df1 > 1000:
            self.cp = ChiSquare_distribution([df2])
        
        
    def pdf(self, x):
        [df1, df2] = self.parvalues
        if self.in_support(x) and (x > 0):
            return self.cBeta.pdf(df1 * x / (df1 * x + df2)) * \
                   df1 * df2 / (df1 * x + df2)**2
        else:
            if (df1 == 2) and (x == 0):
                return 1.0
            else:
                return 0.0

    def cdf(self, x):
        [df1, df2] = self.parvalues
        if self.in_support(x):
            try:
                return self.cBeta.cdf(x * df1 / (df2 + x * df1))
            except:
                if df2 > 1000:
                    return self.cp.cdf(df1 * x)
                elif df1 > 1000:
                    return 1 - self.cp.cdf(df2 / x)
                else:
                    return super().cdf(x)
        else: return super().cdf(x)

    def mean(self):
        df2 = self.parvalues[1]
        return df2 / (df2 - 2) if df2 > 2 else inf

    def var(self):
        [df1, df2] = self.parvalues
        return 2 * self.mean()**2 * (df1 + df2 - 2) / df1 / \
               (df2 - 4) if df2 > 4 else inf

class F_data(Distribution_data):
    distname = ('F', F_distribution)
    parnames = ['d1', 'd2']  # or ['d₁', 'd₂']
    parspecs = [dict(lo=0), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/F_distribution'


# Gumbel_distribution
# ======================

class Gumbel_distribution(Continuous_distribution):
    support = (-inf, inf)

    def pdf(self, x):
        [mu, beta] = self.parvalues
        if self.in_support(x):
            return exp((mu - x) / beta - exp((mu - x) / beta)) / beta
        else:
            return 0

    def cdf(self, x):
        [mu, beta] = self.parvalues
        return exp(-exp((mu - x) / beta))

    def mean(self):
        [mu, beta] = self.parvalues
        return mu + 0.577216 * beta

    def var(self):
        beta = self.parvalues[1]
        return (beta * pi)**2 / 6

class Gumbel_data(Distribution_data):
    distname = ('Gumbel', Gumbel_distribution)
    parnames = ['μ', 'β']
    parspecs = [dict(), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Gumbel_distribution'


# Hypergeometric_distribution
# ===========================
      
class Hypergeometric_distribution(Discrete_distribution):

    @property
    def support(self):
        [N, K, n] = self.parvalues
        return [max(0, n - N + K), min(n, K)]

    def pmf(self, x):
        [N, K, n] = self.parvalues
        if not self.in_support(x):
            return 0
        return exp(log_choose(K, x) + log_choose(N - K, n - x) -
                   log_choose(N, n))

    def mean(self):
        [N, K, n] = self.parvalues
        return n * K / N

    def var(self):
        [N, K, n] = self.parvalues
        return n * K / N * (N - K) / N * (N - n) / (N - 1)

class Hypergeometric_data(Distribution_data):
    distname = ('hypergeometric', Hypergeometric_distribution)
    parnames = ['N', 'K', 'n']
    parspecs = [dict(lo=1, integer=True),
               dict(lo=0, hi='{0}', integer=True),
               dict(lo=0, hi='{0}', integer=True)]
    website = 'http://en.wikipedia.org/wiki/Hypergeometric_distribution'


# Logistic_distribution
# =====================
        
class Logistic_distribution(Continuous_distribution):
    support = (-inf, inf)

    def pdf(self, x):
        [mu, s] = self.parvalues
        if self.in_support(x):
            y = exp((x - mu) / s)
            return 1 / (2 + y + 1 / y) / s
        else:
            return 0

    def cdf(self, x):
        [mu, s] = self.parvalues
        return 1 / (1 + exp((mu - x) / s))

    def mean(self):
        return self.parvalues[0] # = mu

    def var(self):
        s = self.parvalues[1] # = s
        return (s * pi)**2 / 3

class Logistic_data(Distribution_data):
    distname = ('logistic', Logistic_distribution)
    parnames = ['μ', 's']
    parspecs = [dict(), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Logistic_distribution'


# MannWhitney_distribution
# ========================

class MannWhitney_distribution(Discrete_distribution):

    def __init__(self, val):
        super().__init__(val)
        self.parvalues = val
        [M, N] = self.parvalues
        self.frqncy = Wilcoxon_Mann_Whitney_pmf(M, N)

    @property
    def support(self):
        [M, N] = self.parvalues
        return [0, M * N]

    def pmf(self, x):
        if  self.in_support(x):
            return self.frqncy[int(x)]
        else: return 0.0

    def mean(self):
        [M, N] = self.parvalues
        return M * N / 2

    def var(self):
        [M, N] = self.parvalues
        return self.mean() * (M + N + 1) / 6

class MannWhitney_data(Distribution_data):
    distname = ('Mann-Whitney', MannWhitney_distribution)
    parnames = ['M', 'N']
    parspecs = [dict(integer=True, lo=0), dict(integer=True, lo=0)]
    website = 'http://en.wikipedia.org/wiki/Mann-Whitney-Wilcoxon'


# NegativeBinomial_distribution
# =============================

class NegativeBinomial_distribution(Discrete_distribution):

    def pmf(self, x):
        [r, p] = self.parvalues
        if self.in_support(x):
            x = int(x)
            return exp(log_choose(x + r - 1, x) + r * log(1 - p) + x * log(p))
        else: return 0

    def factor(self, i):
        [r, p] = self.parvalues
        if i > 0:
            return (r + i - 1) * (1 - p) / i
        else:
            return 0
        
    def mean(self):
        [r, p] = self.parvalues
        return p * r / (1 - p) 

    def var(self):
        [r, p] = self.parvalues
        return p * r / (1 - p)**2

class NegativeBinomial_data(Distribution_data):   
    distname = ('negative binomial', NegativeBinomial_distribution)
    parnames = ['r', 'p']
    parspecs = [dict(lo=0, integer=True), dict(lo=0, hi=1)]
    note = 'X = number of successes before the r-th failure in a series of independent Bernoulli trials.'
    website = 'http://en.wikipedia.org/wiki/Negative_binomial_distribution'
       
    
# Geometric_distribution
# ======================

class Geometric_distribution(NegativeBinomial_distribution):
    def __init__(self, val):
        p = val[0]
        if p != None:
            super().__init__([1, 1-p]) # Inherit from the negative binomial distribution

class Geometric_data(Distribution_data):
    distname = ('geometric', Geometric_distribution)
    parnames = ['p']
    parspecs = [dict(lo=0, hi=1)]
    note = 'X = number of failures before first success in a series of ' + \
           'independent Bernoulli trials.'
    website = 'http://en.wikipedia.org/wiki/Geometric_distribution'

         
# NonCentralChiSquare_distribution
# ================================
        
class NonCentralChiSquare_distribution(Continuous_distribution):

    def __init__(self, val):
        super().__init__(val)
        self.parvalues = list(val)
        nu = self.parvalues[0]
        self.CentralChi2 = ChiSquare_distribution([nu])

    def in_support(self, x): return x > 0

    def pdf(self, x):
        if not self.in_support(x):
            return 0
        [df, labda] = self.parvalues
        if labda == 0:
            return self.CentralChi2.pdf(x)
        c = labda / 2
        k = round(c)
        pk = exp(-c + k * log(c) - lgamma(k + 1))
        fk = exp(-lgamma(df / 2 + k) - (df / 2 + k) * log(2) - x / 2 +
                 (df / 2 + k - 1) * log(x))
        j = k
        p = pk
        f = fk
        result = pk * fk
        contribution = result
        while (j > 0) and not isclose(contribution, 0, abs_tol = eps):
            j -= 1
            p = p * (j + 1) / c
            f = f * (df + 2 * j) / x
            contribution = p * f
            result = result + contribution
        j = k
        p = pk
        f = fk
        while not isclose(contribution, 0, abs_tol = eps):
            j += 1
            p = p * c / j
            f = f * x / (df + 2 * j - 2)
            contribution = p * f
            result = result + contribution
        return result

    def cdf(self, x):
        if not self.in_support(x):
            return super().cdf(x)
        [df, labda] = self.parvalues
        if labda == 0:
            return self.CentralChi2.cdf(x)
        c = labda / 2
        m = floor(c - 8 * sqrt(c))
        m = int(max(m, 0))
        k = int(c)
        k = max(k, m)
        c_chi2 = ChiSquare_distribution([df + 2 * k])
        Fk = c_chi2.cdf(x)
        pk = exp(-c + k * log(c) - lgamma(k + 1))
        Gk = exp(-x / 2 + (df / 2 + k) * log(x / 2) - lgamma(df / 2 + k + 1))
        result = pk * Fk
        p = pk
        F = Fk
        G = Gk
        sump = 1 - p
        for j in range(k - 1, m - 1, -1):  # for j = k-1 downto m do
            p = p * (j + 1) / c
            sump = sump - p
            G = G * (df / 2 + j + 1) / x * 2
            F = F + G
            result = result + p * F
        p = pk
        F = Fk
        G = Gk
        j = k
        while not isclose(sump * (F - G), 0, abs_tol = eps):
#        while (sump * (F - G) > eps):
            j += 1
            p = p * c / j
            sump = sump - p
            F = F - G
            G = G * x / 2 / (df / 2 + j)
            result = result + p * F
        return result

    def mean(self):
        [nu, labda] = self.parvalues
        return nu + labda

    def var(self):
        [nu, labda] = self.parvalues
        return 2 * (nu + 2 * labda)

class NonCentralChiSquare_data(Distribution_data):
    distname = ('non-central chi-square', NonCentralChiSquare_distribution)
    parnames = ['ν', 'λ']
    parspecs = [dict(lo=0), dict(lo=0, ielo='>=')]
    website = 'http://en.wikipedia.org/wiki/Noncentral_chi-squared_distribution'


# NonCentralBeta_distribution
# ===========================

class NonCentralBeta_distribution(Continuous_distribution):
    support = (0, 1)

    def in_support(self, x): return 0 < x < 1

    def pdf(self, x):
        if not self.in_support(x):
            return 0
        [alpha, beta, labda] = self.parvalues
        c = labda / 2
        m = int(floor(c - 8 * sqrt(c)))
        m = max(m, 0)
        k = int(round(c))
        k = max(k, m)
        if k == 0:
            qk = exp(-c)
        else:
            qk = exp(-c + k * log(c) - lgamma(k + 1))
        sumq = 1 - qk
        fk = exp((alpha + k - 1) * log(x) + (beta - 1) * log(1 - x)
                 + lgamma(alpha + k + beta) - lgamma(alpha + k) - lgamma(beta))
        result = qk * fk  # contribution for j=k
        j = k
        qj = qk
        fj = fk
        while j > m:  # contributions for m <= j < k
            j -= 1
            fj = fj * (alpha + j) / (alpha + beta + j) / x
            qj = qj * (j + 1) / c
            sumq = sumq - qj
            result = result + fj * qj
        j = k
        qj = qk
        fj = fk
        while not isclose(fj * sumq, 0, abs_tol=eps):
#      repeat     #  contributions for j > k
            j += 1
            fj = fj * x * (alpha + beta + j - 1) / (alpha + j - 1)
            qj = qj * c / j
            sumq = sumq - qj
            result = result + fj * qj
#      until (fj*sumq<eps);
        return result

    def cdf(self, x):
        if not self.in_support(x):
            return super().cdf(x)
        [alpha, beta, labda] = self.parvalues
        c = labda / 2
        m = int(floor(c - 8 * sqrt(c)))
        m = max(m, 0)
        k = int(round(c))
        k = max(k, m)
        if k == 0:
            qk = exp(-c)
        else:
            qk = exp(-c + k * log(c) - lgamma(k + 1))
        sumq = 1 - qk
        Gk = exp((alpha + k) * log(x) + beta * log(1 - x)
            + lgamma(alpha + k + beta) - lgamma(alpha + k + 1) - lgamma(beta))
        Ik = incbeta(x, alpha + k, beta)
        result = qk * Ik  # contribution for j=k
        qj = qk
        Gj = Gk
        Ij = Ik
        for j in range(k - 1, m - 1, -1):
# for j = k-1 downto m do   # contribution for j < k
            Gj = Gj * (alpha + j + 1) / (alpha + beta + j) / x
            Ij = Ij + Gj
            qj = qj * (j + 1) / c
            sumq = sumq - qj
            result = result + Ij * qj
        j = k
        qj = qk
        Gj = Gk
        Ij = Ik
        while not isclose((Ij - Gj) * sumq , 0, abs_tol=eps):
#      repeat       # contribution for j > k
            j += 1
            Ij = Ij - Gj
            Gj = Gj * x * (alpha + beta + j - 1) / (alpha + j)
            qj = qj * c / j
            sumq = sumq - qj
            result = result + Ij * qj
#      until ((Ij-Gj)*sumq<eps);
        return result

    def mean(self):
        alpha, beta = self.parvalues[0], self.parvalues[1]
        return alpha / (alpha + beta)

    def var(self):
        alpha, beta = self.parvalues[0], self.parvalues[1]
        return self.mean() * beta / (alpha + beta) / (alpha + beta + 1)

class NonCentralBeta_data(Distribution_data):
    distname = ('non-central beta', NonCentralBeta_distribution)
    parnames = ['α', 'β', 'λ']
    parspecs = [dict(lo=0), dict(lo=0), dict(lo=0, ielo='>=')]
    website = 'https://en.wikipedia.org/wiki/Noncentral_beta_distribution'


# NonCentralF_distribution
# ========================

class NonCentralF_distribution(Continuous_distribution):

    def __init__(self, val):
        super().__init__(val)
        self.parvalues = val
        [nu1, nu2, labda] = self.parvalues
        self.ncBeta = NonCentralBeta_distribution([nu1 / 2, nu2 / 2, labda])

    def pdf(self, x):
        nu1, nu2 = self.parvalues[0], self.parvalues[1]
        if self.in_support(x):
            return self.ncBeta.pdf(nu1 * x / (nu1 * x + nu2)) * \
                   nu1 * nu2 / (nu1 * x + nu2)**2
        else: return 0.0
        
    def cdf(self, x):
        nu1, nu2 = self.parvalues[0], self.parvalues[1]
        if self.in_support(x):
            return self.ncBeta.cdf(x * nu1 / (nu2 + x * nu1))
        else: return super().cdf(x)

    def mean(self):
        [nu1, nu2, labda] = self.parvalues
        if nu2 > 2:
            return nu2 * (nu1 + labda) / (nu1 * (nu2 - 2))
        return inf

    def var(self):
        [nu1, nu2, labda] = self.parvalues
        if nu2 > 4:
            return 2 * (nu2 / nu1)**2 * ((nu1 + labda)**2 + (nu1 + 2 * labda) *\
                                         (nu2 - 2)) / ((nu2 - 2)**2 * (nu2 - 4))
        return inf

class NonCentralF_data(Distribution_data):
    distname = ('non-central F', NonCentralF_distribution)
    parnames = ['ν1', 'ν2', 'λ']  # or ['ν₁','ν₂','λ']
    parspecs = [dict(lo=0), dict(lo=0), dict(lo=0, ielo='>=')]
    website = 'http://en.wikipedia.org/wiki/Noncentral_F-distribution'


# NonCentralT_distribution
# ========================

class NonCentralT_distribution(Continuous_distribution):
    support = (-inf, inf)

    def __init__(self, val):
        super().__init__(val)
        self.parvalues = val
        nu = self.parvalues[0]
        self.centralT = T_distribution([nu])

    def pdf(self, x):
        [df, delta] = self.parvalues
        if delta == 0:
            return self.centralT.pdf(x)
        if not self.in_support(x):
            return 0
        negdelta = delta < 0  # Why distinguish between + and - delta?
        if negdelta:
            x = -x
            delt = -delta
        else:
            delt = delta
        a = delta**2 / 2
        m = int(floor(a - 8 * sqrt(a)))
        m = max(m, 0)
        k = int(round(a))
        k = max(k, m)
        if x == 0:
            k = 0
        b = df / 2
        pk = exp(-a + k * log(a) - lgamma(k + 1))
        if x < 0:
            negx = -1
        else:
            negx = 1
        qk = delt * exp(-a + k * log(a) - 0.5 * log(2) - lgamma(k + 1.5))
        if x == 0:
            if k > 0:
                f1k = 0.0
            else:
                f1k = exp(
                    lgamma(
                        0.5 + b) - lgamma(
                            0.5) - lgamma(
                                b) - 0.5 * log(
                                    df))
            f2k = 0.0
        else:
            c = x**2 / (df + x**2)
            f1k = exp(
                lgamma(
                    k + 0.5 + b) - lgamma(
                        k + 0.5) - lgamma(
                            b)) * pow(
                c,
                     k - 0.5) * pow(
                1 - c,
                         b - 1) * df / (
                x**2 + df)**2 * x
            f2k = exp(
                lgamma(
                    k + 1 + b) - lgamma(
                        k + 1) - lgamma(
                            b)) * pow(
                c,
                     k) * pow(
                1 - c,
                         b - 1) * df / (
                x**2 + df)**2 * x
        p = pk
        q = qk
        f1 = f1k
        f2 = f2k
        result = negx * p * f1 + q * f2
        for j in range(k - 1, m - 1, -1):
            p = p * (j + 1) / a
            q = q * (j + 1.5) / a
            if x != 0:
                f1 = f1 * (x**2 + df) / x**2 * (j + 0.5) / (j + 0.5 + b)
                f2 = f2 * (x**2 + df) / x**2 * (j + 1) / (j + 1 + b)
            result = result + negx * p * f1 + q * f2
        p = pk
        q = qk
        f1 = f1k
        f2 = f2k
        j = k
        while not isclose(q * f2, 0, abs_tol=eps):
            j += 1
            p = p * a / j
            q = q * a / (j + 0.5)
            f1 = f1 * x**2 / (x**2 + df) * (j - 0.5 + b) / (j - 0.5)
            f2 = f2 * x**2 / (x**2 + df) * (j + b) / j
            result = result + negx * p * f1 + q * f2
        return result

    def cdf(self, x):
        '''
#       Based on Algorithm AS 243:
#		Russell V. Lenth:
#		Cumulative Distribution Function of the Non-central t Distribution,
#		Applied Statistics (1989), Vol. 38, No.1, pp. 185 -189.
        '''
        [df, delta] = self.parvalues
        if delta == 0:
            return self.centralT.cdf(x)
        negdelta = delta < 0  # Why distinguish between + en - delta?
        if negdelta:
            x = -x
            delt = -delta
        else:
            delt = delta
        a = delt**2 / 2
        m = int(floor(a - 8 * sqrt(a)))
        m = max(m, 0)
        k = int(round(a))
        k = max(k, m)
        b = df / 2
        pk = 0.5 * exp(-a + k * log(a) - lgamma(k + 1))
        if x < 0:
            negx = -1
        else:
            negx = 1
        qk = 0.5 * delt * exp(-a + k * log(a) - 0.5 * log(2) - lgamma(k + 1.5))
        x = x**2 / (x**2 + df)
        I1k = incbeta(x, k + 0.5, b)
        I2k = incbeta(x, k + 1, b)
        G1k = exp(
            lgamma(
                k + 0.5 + b) - lgamma(
                    k + 1.5) - lgamma(
                b)) * pow(
                    x,
                     k + 0.5) * pow(
            1 - x,
                         b)
        G2k = exp(
            lgamma(
                k + 1 + b) - lgamma(
                    k + 2) - lgamma(
                b)) * pow(
                    x,
                     k + 1) * pow(
            1 - x,
                         b)
        p = pk
        q = qk
        I1 = I1k
        I2 = I2k
        G1 = G1k
        G2 = G2k
        sump = 0.5 - p
        result = 0.5 * \
            (1 - incgamma((delt**2) / 2, 0.5)) + negx * p * I1 + q * I2
#	result = 0.5*(1+erf(-del/sqrt(2)))+negx*p*I1+q*I2;
        for j in range(k - 1, m - 1, -1):  # = k-1 downto m do
            p = p * (j + 1) / a
            q = q * (j + 1.5) / a
            sump = sump - p
            if x > 0:
                G1 = G1 * (j + 1.5) / (j + 0.5 + b) / x
            if x > 0:
                G2 = G2 * (j + 2) / (j + 1 + b) / x
            I1 = I1 + G1
            I2 = I2 + G2
            result = result + negx * p * I1 + q * I2
        p = pk
        q = qk
        I1 = I1k
        I2 = I2k
        G1 = G1k
        G2 = G2k
        j = k
        while not isclose(sump * (I1 - G1) * 2, 0, abs_tol=0.5*eps):
            j += 1
            p = p * a / j
            q = q * a / (j + 0.5)
            sump = sump - p
            I1 = I1 - G1
            I2 = I2 - G2
            result = result + negx * p * I1 + q * I2
            G1 = G1 * x * (j - 0.5 + b) / (j + 0.5)
            G2 = G2 * x * (j + b) / (j + 1)
        if negdelta:
            result = 1 - result
        return result

    def mean(self):
        [nu, mu] = self.parvalues
        if nu > 1:
            return mu * sqrt(nu / 2) * exp(lgamma((nu - 1) / 2) 
                                           - lgamma(nu / 2))
        return inf

    def var(self):
        [nu, mu] = self.parvalues
        if nu > 2:
            return nu / (nu - 2) * (1 + mu**2) - self.mean()**2
        return inf

class NonCentralT_data(Distribution_data):
    distname = ('non-central t', NonCentralT_distribution)
    parnames = ['ν', 'μ']
    parspecs = [dict(lo=0), dict()]
    website = 'http://en.wikipedia.org/wiki/Noncentral_t-distribution'

 
# Normal_distribution
# ===================

class Normal_distribution(Continuous_distribution):
    support = (-inf, inf)

    def __init__(self, val):
        super().__init__(val)
        self.mu, self.sigma = self.parvalues[0], sqrt(self.parvalues[1])

    def pdf(self, x):
        z = (x - self.mu) / self.sigma
        return exp(-z**2 / 2.0) / sqrt(2 * pi)

    def cdf(self, x):
        z = (x - self.mu) / self.sigma
        return 0.5 + 0.5 * erf(z / sqrt(2.0))

    def mean(self):
        return self.mu

    def var(self):
        return self.sigma**2

class Normal_sigma_distribution(Normal_distribution):

    def __init__(self, val):
        if val[0] != None and val[1] != None:
            super().__init__((val[0],val[1]**2))

class Normal_data(Distribution_data):
    distname = ('normal (μ,σ²)', Normal_distribution)
    parnames = ['μ', 'σ²']
    parspecs = [dict(), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/normal_distribution'

class Normal_data_sigma(Distribution_data):
    distname = ('normal (μ,σ)', Normal_sigma_distribution)
    parnames = ['μ', 'σ']
    parspecs = [dict(), dict(lo=0)]  
    website = 'http://en.wikipedia.org/wiki/normal_distribution'


# FoldedNormal_distribution
# =========================

class FoldedNormal_distribution(Continuous_distribution):

    def __init__(self, val):
        super().__init__(val)
        self.standardNormal = Normal_distribution([0, 1])
    
    def pdf(self, x):
        [mu, sigma2] = self.parvalues
        sigma = sqrt(sigma2)
        if not self.in_support(x):
            return 0
        z1, z2 = (x - mu) / sigma, (x + mu) / sigma
        return self.standardNormal.pdf(z1) + self.standardNormal.pdf(z2)

    def cdf(self, x):
        [mu, sigma2] = self.parvalues
        if self.in_support(x):
            return 0.5 * (erf((x + mu) / sqrt(2 * sigma2)) +
                          erf((x - mu) / sqrt(2 * sigma2)))
        else: return super().cdf(x)

    def mean(self):
        [mu, sigma2] = self.parvalues
        sigma = sqrt(sigma2)
        return sqrt(2 * sigma2 / pi) * exp(-mu**2 / (2 * sigma2))\
               + mu * (1 - 2 * self.standardNormal.cdf(-mu / sigma))

    def var(self):
        [mu, sigma2] = self.parvalues
        return mu**2 + sigma2 - self.mean()**2

class FoldedNormal_data(Distribution_data):
    distname = ('folded-normal', FoldedNormal_distribution)
    parnames = ['μ', 'σ²']
    parspecs = [dict(), dict(lo=0)]
    note = 'distribution of |X| if X is normal(μ, σ²).'
    website = 'http://en.wikipedia.org/wiki/Folded_normal_distribution'


# LogNormal_distribution
# ======================

class LogNormal_distribution(Continuous_distribution):

    def __init__(self, val):
        super().__init__(val)
        self.standardNormal = Normal_distribution([0, 1])

    def in_support(self, x): return x > 0

    def pdf(self, x):
        [mu, sigma2] = self.parvalues
        constant = -(log(2 * pi) + log(sigma2)) / 2
        if self.in_support(x):
            return exp(constant - log(x) - (log(x) - mu)**2 / sigma2 / 2)
        else: return 0.0

    def cdf(self, x):
        if self.in_support(x):
            return self.standardNormal.cdf(log(x))
        else: return super().cdf(x)

    def mean(self):
        [mu, sigma2] = self.parvalues
        return exp(mu + sigma2 / 2)

    def var(self):
        [mu, sigma2] = self.parvalues
        return exp(2 * mu + 2 * sigma2) - \
               exp(2 * mu + sigma2)

class LogNormal_data(Distribution_data):
    distname = ('log-normal', LogNormal_distribution)
    parnames = ['μ', 'σ²']
    parspecs = [dict(), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Log-normal_distribution'


# InverseGaussian_distribution
# ============================

class InverseGaussian_distribution(Continuous_distribution):

    def __init__(self, val):
        super().__init__(val)
        self.standardNormal = Normal_distribution([0, 1])

    def in_support(self, x): return x > 0

    def pdf(self, x):
        [labda, mu] = self.parvalues
        if self.in_support(x):
            return sqrt(labda / (2 * pi * x**3)) * exp(-labda *
                                        (x - mu)**2 / (2 * mu**2 * x))
        else: return 0.0

    def cdf(self, x):
        [labda, mu] = self.parvalues
        if self.in_support(x):
            phi = labda / mu
            return self.standardNormal.cdf(sqrt(labda / x) * (x / mu - 1)) +\
                   exp(2 * phi) * self.standardNormal.cdf(-sqrt(labda / x) *
                                                    (x / mu + 1))
        else: return super().cdf(x)

    def mean(self):
        return self.parvalues[0] # = labda

    def var(self): 
        [labda, mu] = self.parvalues
        return mu**3 / labda

class InverseGaussian_data(Distribution_data):
    distname = ('inverse gaussian', InverseGaussian_distribution)
    parnames = ['λ', 'μ']
    parspecs = [dict(lo=0), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Inverse_Gaussian_distribution'


# Pareto_distribution
# ===================

class Pareto_distribution(Continuous_distribution):
    
    @property
    def support(self):
        alpha = self.parvalues[1]
        return (alpha, inf)
        
    def pdf(self, x):
        [xm, alpha] = self.parvalues
        if  self.in_support(x):
            return xm / alpha * pow(alpha / x, xm + 1)
        else: return 0.0

    def cdf(self, x):
        [xm, alpha] = self.parvalues
        if self.in_support(x):
            return 1 - pow(alpha / x, xm)
        else: return super().cdf(x)

    def mean(self):
        [xm, alpha] = self.parvalues
        if xm > 1:
            return alpha * xm / (xm - 1)
        else:
            return inf

    def var(self):
        [xm, alpha] = self.parvalues
        if xm > 2:
            return self.mean() * alpha / ((xm - 1) * (xm - 2))
        else:
            return inf

class Pareto_data(Distribution_data):
    distname = ('Pareto', Pareto_distribution)
    parnames = ['xm', 'α']  # or ['xₘ', 'α']
    parspecs = [dict(lo=0), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Pareto_distribution'

 
# Poisson_distribution
# ====================

class Poisson_distribution(Discrete_distribution):

    def pmf(self, x):
        [labda] = self.parvalues
        if self.in_support(x):
            ix = int(x)
            if labda == 0:
                if ix == 0:
                    return 1
                else:
                    return 0
            else:
                return exp(ix * log(labda) - labda - lgamma(ix + 1))
        else: return 0.0
        
    def cdf(self, x):
        [labda] = self.parvalues
        if self.support[0] <= x < self.support[1]:
            x = round(x) if isclosetoint(x) else floor(x)
            return 1 - incgamma(labda, x + 1)
        else: return super().cdf(x)

    def mean(self):
        return self.parvalues[0]

    def var(self):
        return self.parvalues[0]

class Poisson_data(Distribution_data):
    distname = ('Poisson', Poisson_distribution)
    parnames = ['λ']
    parspecs = [dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Poisson_distribution'

 
# T_distribution
# ==============

class T_distribution(Continuous_distribution):
    support = (-inf, inf)

    def __init__(self, val):
        super().__init__(val)
        self.parvalues = val
        self.N = Normal_distribution([0, 1])
        
    def pdf(self, x):
        [nu] = self.parvalues
        constant = lgamma((nu + 1) / 2) - lgamma(nu / 2) - log(nu * pi) / 2
        return exp(constant + (nu + 1) / 2 * log(nu / (nu + x**2)))

    def cdf(self, x):
        [nu] = self.parvalues
        try:
            if x >= 0:
                return 1 - 0.5 * incbeta(nu / (nu + x**2), nu / 2, 1 / 2)
            else:
                return 0.5 * incbeta(nu / (nu + x**2), nu / 2, 1 / 2)
        except:
            if x <= 0:
                return(1 - incgamma(x**2 / 2, 0.5)) / 2
            else:
                return (1 + incgamma(x**2 / 2, 0.5)) / 2

    def mean(self):
        if self.parvalues[0] > 1:
            return 0
        else:
            return inf

    def var(self):
        [nu] = self.parvalues
        if nu > 2:
            return nu / (nu - 2)
        else:
            return inf

class T_data(Distribution_data):
    distname = ('t', T_distribution)
    parnames = ['ν']
    parspecs = [dict(lo=0)]
    website = "http://en.wikipedia.org/wiki/Student's_t-distribution"

 
# Triangular_distribution
# =======================

class Triangular_distribution(Continuous_distribution):
    
    @property
    def support(self):
        a, b = self.parvalues[0], self.parvalues[2]
        return (a, b)

    def pdf(self, x):
        [a, c, b] = self.parvalues
        if self.in_support(x):
            if x <= c:
                return 2 * (x - a) / ((b - a) * (c - a))
            else:
                return 2 * (b - x) / ((b - a) * (b - c))
        else: return 0.0

    def cdf(self, x):
        [a, c, b] = self.parvalues
        if self.in_support(x):
            if x <= c:
                return (x - a)**2 / ((b - a) * (c - a))
            else:
                return 1 - (b - x)**2 / ((b - a) * (b - c))
        else: return super().cdf(x)

    def mean(self):
        [a, c, b] = self.parvalues
        return (a + b + c) / 3

    def var(self):
        [a, c, b] = self.parvalues
        return (a**2 + b**2 + c**2 - a * b - a * c - b * c) / 18

class Triangular_data(Distribution_data):
    distname = ('triangular', Triangular_distribution)
    parnames = ['a', 'c', 'b']
    parspecs = [dict(), dict(lo='{0}'), dict(lo='{1}')]
    website = 'http://en.wikipedia.org/wiki/Triangular_distribution'


# Uniform_distribution
# ====================

class Uniform_distribution(Continuous_distribution):
    
    @property
    def support(self):
        return self.parvalues

    def pdf(self, x):
        [a, b] = self.parvalues
        if self.in_support(x):
            return 1 / (b - a)
        else: return 0.0

    def cdf(self, x):
        [a, b] = self.parvalues
        if self.in_support(x):
            return (x - a) / (b - a)
        else: return super().cdf(x)

    def mean(self):
        [a, b] = self.parvalues
        return (a + b) / 2

    def var(self):
        [a, b] = self.parvalues
        return (b - a)**2 / 12

class Uniform_data(Distribution_data):
    distname = ('uniform', Uniform_distribution)
    parnames = ['a', 'b']
    parspecs = [dict(), dict(lo='{0}')]
    website = 'http://en.wikipedia.org/wiki/Uniform_distribution_(continuous)'


# Weibull_distribution
# ====================

class Weibull_distribution(Continuous_distribution):

    def pdf(self, x):
        [labda, k] = self.parvalues
        if self.in_support(x) or x == 0:
            return k / labda * exp((k - 1) * log(x / labda)) \
                   * exp(-pow(x / labda, k))
        else: return 0.0

    def cdf(self, x):
        [labda, k] = self.parvalues
        if self.in_support(x) or x == 0:
            return 1 - exp(-pow(x / labda, k))
        else: return super().cdf(x)

    def mean(self):
        [labda, k] = self.parvalues
        return labda * exp(lgamma(1 + 1 / k))

    def var(self):
        [labda, k] = self.parvalues
        return labda**2 * (exp(lgamma(1 + 2 / k)) - \
                               (exp(lgamma(1 + 1 / k)))**2)

class Weibull_data(Distribution_data):
    distname = ('Weibull', Weibull_distribution)
    parnames = ['λ', 'k']
    parspecs = [dict(lo=0), dict(lo=0)]
    website = 'http://en.wikipedia.org/wiki/Weibull_distribution'


# WilcoxonRankSum_distribution
# ============================

class WilcoxonRankSum_distribution(Discrete_distribution):

    def __init__(self, val):
        super().__init__(val)
        [M, N] = self.parvalues
        self.shift = (M * (M + 1)) // 2
        self.frqncy = Wilcoxon_Mann_Whitney_pmf(M, N)

    @property
    def support(self):
        [M, N] = self.parvalues
        return [self.shift, M * N + self.shift]
        
    def pmf(self, x):
        if self.in_support(x):
            return self.frqncy[int(x) - self.shift]
        else: return 0.0
        
    def mean(self):
        [M, N] = self.parvalues
        return M * N / 2 + self.shift

    def var(self):
        [M, N] = self.parvalues
        return self.mean() * (M + N + 1) / 6

class WilcoxonRankSum_data(Distribution_data):
    distname = ('Wilcoxon rank-sum', WilcoxonRankSum_distribution)
    parnames = ['M', 'N']
    parspecs = [dict(integer=True, lo=0), dict(integer=True, lo=0)]
    website = 'http://en.wikipedia.org/wiki/Mann-Whitney-Wilcoxon'


# WilcoxonSignedRank_distribution
# ===============================

class WilcoxonSignedRank_distribution(Discrete_distribution):

    def __init__(self, val):
        super().__init__(val)
        n = self.parvalues[0] = int(self.parvalues[0])
        N = int(n * (n + 1) // 2)
        mid = int(round(N / 2 - 0.2))
        P = [0 for i in range(N + 1)]
        P[0] = 1
        for m in range(1, n + 1):
            k = ((m - 1) * m) // 2
            k = min(k, mid - m)
            for i in range(k + 1):
                P[m + k - i] = P[m + k - i] + P[k - i]
        total = 2**n
        for i in range(mid + 1):
            P[i] = P[i]/total
        for i in range(mid + 1, N + 1):
            P[i] = P[N - i]
        self.P = P

    @property
    def support(self):
        n = self.parvalues[0]
        N = int(n * (n + 1) // 2)
        return [0, N]

    def pmf(self, x):
        if self.in_support(x):
            return self.P[int(x)]
        else: return 0.0

    def mean(self):
        return self.support[1] / 2

    def var(self):
        n = self.parvalues[0]
        return self.mean() * (2 * n + 1) / 6

class WilcoxonSignedRank_data(Distribution_data):
    distname = ('Wilcoxon signed rank', WilcoxonSignedRank_distribution)
    parnames = ['N']
    parspecs = [dict(integer=True, lo=0)]
    website = 'http://en.wikipedia.org/wiki/Wilcoxon_signed-rank_test'
    

distributions = {}
for cls in Distribution_data.__subclasses__():
    distributions[cls.distname[0]] = cls
   
# ======================

if __name__ == '__main__':
    dat = Normal_data(0, 1) # data for the standard normal distribution
    print('tuple of distribution name and class: ', dat.distname)
    print('list of parameter names: ', dat.parnames)
    print('list of parameter specifications: ', dat.parspecs)
    print('list of parameter values: ', dat.parvalues)
    print('number of parameters: ', dat.npars)
    print('list of hints for the parameters: ', dat.parhints)
    print('list of parameters with unknown value: ', dat.unknown_parameters())
    print('are the values of all parameters known? ', dat.all_known())
    print('is it a continuous distribution? ', dat.iscontinuous)
    print('is it a discrete distribution? ', dat.isdiscrete)
    print('a note (about entering a parameter value): ', dat.note)
    print('a reference to the relevant wikipedia website: ', dat.website)
    rv = dat.rv # or: rv = Normal_distribution([0, 1])
    print('rv.parvalues: ', rv.parvalues)
    print('support: ', rv.support)
    print('does a given value (0) belong to the support? ', rv.in_support(0))
    print('print the support: ', rv.show_support())
    print('probability density function for a given x-value (0): ', rv.pdf(0))
    print('probability (mass function) for a given x-value (0): ', rv.pmf(0))
    print('cumulative distribution function for a given x-value(1.96)', rv.cdf(1.96))
    print('quantile or inverse distribution function for a given probability (0.95): ', rv.quantile(0.95))
    print('median: ', rv.median())
    print('expected value: ', rv.mean())
    print('variance: ', rv.var())
    print('standard deviation: ', rv.std())

    dat = Geometric_data(0.3)
    print('tuple of distribution name and class: ', dat.distname)
    print('list of parameter names: ', dat.parnames)
    print('list of parameter specifications: ', dat.parspecs)
    print('list of parameter values: ', dat.parvalues)
    print('number of parameters: ', dat.npars)
    print('list of hints for the parameters: ', dat.parhints)
    print('list of parameters with unknown value: ', dat.unknown_parameters())
    print('are the values of all parameters known? ', dat.all_known())
    print('is it a continuous distribution? ', dat.iscontinuous)
    print('is it a discrete distribution? ', dat.isdiscrete)
    print('a note (about entering a parameter value): ', dat.note)
    print('a reference to the relevant wikipedia website: ', dat.website)
    
    rv = dat.rv # or: rv = Geometric_distribution([0.3])
    print('rv.parvalues: ', rv.parvalues)
    print('support: ', rv.support)
    print('does a given value (0) belong to the support? ', rv.in_support(0))
    print('print the support: ', rv.show_support())
    print('probability density function for a given x-value (0): ', rv.pdf(0))
    print('probability (mass function) for a given x-value (0): ', rv.pmf(0))
    print('cumulative distribution function for a given x-value(1.96)', rv.cdf(1.96))
    print('quantile or inverse distribution function for a given probability (0.95): ', rv.quantile(0.95))
    print('median: ', rv.median())
    print('expected value: ', rv.mean())
    print('variance: ', rv.var())
    print('standard deviation: ', rv.std())    

    for distribution in distributions:
        print(distribution) 
