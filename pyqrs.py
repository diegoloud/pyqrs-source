'''
This is the main module of PyQRS.
The name stands for Probabilities, Quantiles and Random Samples, where a 'y'
is inserted as a tribute to the programming language Python.
This module covers, together with the file pyqrs.kv, the PyQRS user interface.
The Kivy framework (https://kivy.org) is used for the graphical user interface.
'''

__version__ = 4.2

from kivy.app import App
from kivy.properties import ObjectProperty, NumericProperty,\
        BoundedNumericProperty, StringProperty, ListProperty, BooleanProperty
from kivy.core.window import Window
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.clock import Clock
from kivy.metrics import dp
from platform import uname
from math import isinf, floor, ceil, log10, copysign
from kivy_garden.graph import Graph, MeshStemPlot, LinePlot
from pyqrs_slider import Slider2
import distrib as d
import pyqrs_graphics as gr


# From kivy_garden.graph the following three Graph class methods were adapted:
Graph._get_ticks = gr._new_get_ticks
Graph._update_ticks = gr._new_update_ticks
Graph.to_window = gr.to_window

class PyQRSScreen(TabbedPanel):
    '''
    This class is at the heart of PyQRS. It contains, accompanied by
    pyqrs.kv, the logic for the user interface. The Properties in the list
    below guarantee that changes in a property are immediately carried through.
    '''
    dat = ObjectProperty(None, allownone=True, rebind=True)
                               # refers to a data object from the distrib module
    rv = ObjectProperty(None, allownone=True, rebind=True)
                       # refers to a distribution object from the distrib module
    xmin = NumericProperty(None, allownone=True)               # minimal x-value
    xmax = NumericProperty(None, allownone=True)               # maximum x-value
    xval1 = NumericProperty(None, allownone=True)               # actual x_value
    xval2 = NumericProperty(None, allownone=True)        # second actual x_value
    pxfunction = ListProperty([])             # list of pmf-values or pdf-values
    cdfunction = ListProperty([])                           # list of cdf-values
    cdfval = NumericProperty(None, allownone=True)   # cdf(x) for actual x-value
    pxfplots = ListProperty([], allownone=True)     # list of points for pmf/pdf
    cdfplots = ListProperty([], allownone=True)             # list of cdf-points
    cdf_graph = ObjectProperty(None, allownone=True)              # graph of cdf
    Px = NumericProperty(0, allownone=True)              # P(X = actual x-value)
    ndecp = NumericProperty(4, max=9)     # number of decimals for probabilities
    ndecx = NumericProperty(3, max=9, errorvalue=3)    # of decimals for x-value
    note = StringProperty('')        # a note about the probability distribution
    Nsample = BoundedNumericProperty(10, min = 1, max = 1000000, errorvalue = 1)
    isdiscrete = BooleanProperty(False)       # True if distribution is discrete
    iscontinuous = BooleanProperty(True)    # True if distribution is continuous
    forMobileDevice = BooleanProperty(True)              # True if mobile device
    editheight = dp(0.05)                      # height of edits on page 2 and 3
    xeditwidth = dp(0.1)                # width of x-value edits on page 2 and 3
    peditwidth = dp(0.1)            # width of probability edits on page 2 and 3
    lw = NumericProperty(3)               # line width in graphs on page 2 and 3

    def __init__(self):
        '''
        Initialise PyQRSScreen(TabbedPanel).
        Start with a standard normal distribution
        '''
        self.dat = d.Normal_data(0, 1) # creates instance of a Normal_data class
        super(PyQRSScreen, self).__init__()
        self.new_parameter() # parameters are (0, 1) (standard normal)
        self.forMobileDevice = uname()[4].startswith('arm') \
			or uname()[4].startswith('aarch64') # 'arm' device 
        Window.softinput_mode = 'below_target' # for mobile touch devices


# Auxiliary methods
# =================

    def warning(self, title='Value error', text= 'No number', id='cdf'):
        ''' displays a warning in the form of a popup '''
        
        # without the following line a warning would erroneously 
        # also be displayed when _entering_ a TextInput
        if not self.ids[id].focus: 
            Popup(title = title,
                  content = Label(text=text),
                  size_hint = (0.4, 0.2),
                  separator_color = [125/255., 159/255., 195/255., 1],
                  title_size = 18,
                  title_align = 'center').open()

 
    def update_note(self):
        '''
        The string self.note consists of a possible rv.note, support and moments.
        Called in new_distribution and SetrvValues and displayed on 1st screen.
        If support is infinite, 'inf' is replaced by '∞'.
        If a moment is infinite, 'inf' is replaced by 'does not exist'.
        '''
        result = '[b]Note:[/b] [i]' + self.dat.note + '[/i]\n' \
                    if self.dat.note else ''
        if self.dat.all_known():
            result += '\nSupport = '+ self.rv.show_support().replace('inf', '∞') \
                +'\nExpected value = '+self.show(self.rv.mean(),self.ndecx) \
                +'\nVariance = '+self.show(self.rv.var(),self.ndecx) \
                +'\nStandard deviation = '+self.show(self.rv.std(),self.ndecx)
            self.note = result.replace('= inf', 'does not exist')

    def show(self, value, ndec):
        ''' formatting the display of float values '''
        return str('{0:.' + str(ndec) + 'f}').format(value)

    def update_edits(self):
        ''' position and redraw edit fields, called from pyqrs.kv '''
        Clock.schedule_once(self.reposition_edits, 0.01)

    def reposition_edits(self, *args):
        '''
        Input edits are placed in correct position w.r.t. graph axes.
        Called from new_edit_value, SetrvValues, switch_to.
        '''
        if not self.rv: # nothing to reposition
            return
        shift = dp(0.005)     # amount of space between xi_pxf fields and x-axis
        field1 = self.ids.x1_pxf        # edit field for the input of an x-value
        field2 = self.ids.x2_pxf                 # edit field for second x-value
        
        xwmin_pxf, yw0 = self.ids.pxf_graph.to_window(self.xmin, 0)
        xwmax_pxf, yw0 = self.ids.pxf_graph.to_window(self.xmax, 0) 

        xw1, yw0 = self.ids.pxf_graph.to_window(self.xval1, 0) 
                                       # xw1 = self.xval1 in window co-ordinates
        xw1 = max(0, min(1, xw1))                    # ensure that 0 <= xw1 <= 1

        xw2, yw0 = self.ids.pxf_graph.to_window(self.xval2, 0)  
        xw2 = max(0, min(1, xw2))                    # ensure that 0 <= xw2 <= 1
        
        # x1_pxf, x2_pxf
        delta = field1.width / self.ids.pxf_graph.width         # relative width
        xw = (xw1 + xw2) / 2
        if 0 < xw2 - xw1 < delta:             # collision of the two edit fields
            field2.pos_hint = {
                'x': max(min(1 - delta, xw), delta),
                'top': yw0 - shift} 
            field1.pos_hint = {
                'right': min(max(delta, xw), 1 - delta),
                'top': yw0 - shift}
        else:
            field2.pos_hint = {
                'center_x': max(min(1 - delta / 2, xw2), 1.5 * delta),
                'top': yw0 - shift}
            field1.pos_hint = {
                'center_x': min(max(delta / 2, xw1), 1 - 1.5 * delta),
                'top': yw0 - shift}
                    
        # red, white, blue fields
        red = self.ids.probleft
        white = self.ids.probcentre
        blue = self.ids.probright
        delta = white.width / self.ids.pxf_graph.width    
        xw = min(max(1.5 * delta, xw), 1 - 1.5 * delta)
        yw = 0.1 + 0.9 * yw0  
        white.pos_hint = {'center_x': xw, 'y': yw}
        xw1 = max(delta / 2, min((0 + 2 * xw1) / 3, xw - delta))
        xw2 = min(1 - delta / 2, max((1 + 2 * xw2) / 3, xw + delta))
        red.pos_hint = {'center_x': xw1, 'y': yw}
        blue.pos_hint = {'center_x': xw2, 'y': yw}

        # slider pxf                                      
        self.ids.id_slider_pxf.pos_hint = {'x':xwmin_pxf, 'bottom': 0}
        if not self.forMobileDevice:                                      
            self.ids.id_slider_pxf.size_hint = \
                        xwmax_pxf - xwmin_pxf, yw0 - field1.height/self.height

        # x1_cdf, cdf
        x1_cdf, y_cdf = self.ids.x1_cdf, self.ids.cdf
        delta = x1_cdf.width / self.ids.cdf_graph.width   
        xw, yw = self.ids.cdf_graph.to_window(self.xval1, 0)
        xw = max(delta / 2, min(1 - delta / 2, xw))
        x1_cdf.pos_hint = {'center_x': xw, 'top': yw - shift}
        
        basis = x1_cdf.height / self.ids.cdf_graph.height / 2 + yw
        xwmin_cdf, yw = self.ids.cdf_graph.to_window(self.xmin, self.cdfval)
        xwmax_cdf, yw = self.ids.cdf_graph.to_window(self.xmax, self.cdfval)
        y_cdf.pos_hint = {'left': 0, 'center_y': max(basis, yw)}

        # slider cdf                                    
        self.ids.id_slider_cdf.pos_hint = {'x':xwmin_cdf, 'bottom': 0}
        if not self.forMobileDevice:                                      
            self.ids.id_slider_cdf.size_hint = \
                        xwmax_cdf - xwmin_cdf, yw0 - field1.height/self.height

        # finishing: 
        if self.xmin <= self.xval1 <= self.xmax:
            self.ids.id_slider_pxf.value = self.xval1
            self.ids.id_slider_pxf.value2 = self.xval2
            self.ids.id_slider_cdf.value = self.xval1


    def draw_pxf(self):
        '''
        Draw pdf or pmf on page 2 (pdf/pmf).
        Called from new_edit_value, SetrvValues, reposition_edits.
        '''
        
        def addpillar(aplot, x, y):
            '''
            function for the construction of one vertical stake in the pmf
            '''
            aplot.points.append((x - 0.5, 0))
            aplot.points.append((x, 0))
            aplot.points.append((x, y))
            aplot.points.append((x, 0))
            aplot.points.append((x + 0.5, 0))
            
        xmin, xmax = self.xmin, self.xmax
        self.ids.pxf_graph.xmin = xmin
        self.ids.pxf_graph.xmax = xmax
        self.ids.pxf_graph.ymax = 1.1 * max([y for (x,y) in self.pxfunction])
        
        if self.isdiscrete:                              # discrete distribution
            if self.forMobileDevice:                           # lw = line width
                lw = self.lw = min(Window.width /(self.xmax - self.xmin) / 5, 5)
            else:
                lw = self.lw = min(Window.width /(self.xmax - self.xmin) / 5, 3)
            if not self.plotleft:
                self.plotleft = LinePlot(color=[1, 0, 0, 1], line_width=lw)
                self.plotx = LinePlot(color=[1, 1, 1, 1], line_width=lw) 
                self.plotright = LinePlot(color=[0, 0, 1, 1], line_width=lw)
                self.pxfplots.append(self.plotleft)
                self.pxfplots.append(self.plotright)
                self.pxfplots.append(self.plotx)

                self.ids.pxf_graph.add_plot(self.plotleft)
                self.ids.pxf_graph.add_plot(self.plotright)
                self.ids.pxf_graph.add_plot(self.plotx)
            self.plotleft.points = []
            self.plotright.points = []
            self.plotx.points = []
            for (x, y) in self.pxfunction:
                if x < self.xval1:
                    addpillar(self.plotleft, x, y)
                elif x > self.xval2:
                    addpillar(self.plotright, x, y)
                else:
                    addpillar(self.plotx, x, y)

        else:   # continuous distribution
            ival1 = int(round((self.xval1 - xmin) / (xmax - xmin) * 600))
            ival1 = max(0, min(ival1, 600))    # integer value between 0 and 600
            ival2 = int(round((self.xval2 - xmin) / (xmax - xmin) * 600))
            ival2 = max(0, min(ival2, 600))    # integer value between 0 and 600
            if not self.pxfplots:
                self.plotleft   = MeshStemPlot(mode='lines', color=[0.9, 0.1, 0.1, 1])
                self.plotcentre = MeshStemPlot(mode='lines', color=[0.9, 0.9, 0.9, 1])
                self.plotright  = MeshStemPlot(mode='lines', color=[0.1, 0.1, 0.9, 1])
                self.plotx = LinePlot(color=[0.1, 0.1, 0.1, 1], line_width=1)                
                self.densityplot =  LinePlot(color=[0.1, 0.1, 0.1, 1], line_width=1)
                self.densityplot.points = self.pxfunction
                self.pxfplots.append(self.densityplot)

            self.plotleft.points = self.pxfunction[:(ival1 + 1)]
            self.pxfplots.append(self.plotleft)
            self.ids.pxf_graph.add_plot(self.plotleft)

            self.plotright.points = self.pxfunction[ival2:]
            self.pxfplots.append(self.plotright)
            self.ids.pxf_graph.add_plot(self.plotright)

            self.plotcentre.points = self.pxfunction[(ival1 + 1):ival2]
            self.pxfplots.append(self.plotcentre)
            self.ids.pxf_graph.add_plot(self.plotcentre)

            self.plotx.points = (
                self.pxfunction[ival1], (self.pxfunction[ival1][0], 0),
                    (self.pxfunction[ival2][0], 0), self.pxfunction[ival2])
            self.pxfplots.append(self.plotx)
            self.ids.pxf_graph.add_plot(self.plotx)

            self.ids.pxf_graph.add_plot(self.densityplot)
            self.ids.pxf_graph.add_x_axis(xmin, xmax, xlog=False)            
            self.plotxaxis = LinePlot(color=[0.1, 0.1, 0.1, 1], line_width=2)
            self.plotxaxis.points = [(xmin-0.5,0), (xmax+0.5,0)]
            self.pxfplots.append(self.plotxaxis)
            self.ids.pxf_graph.add_plot(self.plotxaxis)


    def draw_cdf(self):
        '''
        Draw the cumulative distribution function on page 3 (cdf).
        Called from new_edit_value, SetrvValues
        '''
        self.ids.cdf_graph.xmin = xmin = self.xmin
        self.ids.cdf_graph.xmax = xmax = self.xmax
        if not self.cdfplots:
            if self.isdiscrete: # discrete distribution
                for (x, y) in self.cdfunction:
                    plot = LinePlot(color=[1, 0, 0, 1], line_width=2)
                    plot.points = [(x, y), (x + 1, y)]
                    self.cdfplots.append(plot)
                    self.ids.cdf_graph.add_plot(plot)
            else:   # continuous distribution:
                plot = LinePlot(color=[1, 0, 0, 1], line_width=2)
                plot.points = self.cdfunction
                self.cdfplots.append(plot)
                self.ids.cdf_graph.add_plot(plot)
            self.hairlineplot = LinePlot(color=[0.1, 0.1, 0.1, 1], line_width=1.5)
        self.hairlineplot.points = [(self.xval1, -dp(0.025)), (self.xval1, self.cdfval),
                                    (xmin, self.cdfval)]
        self.cdfplots.append(self.hairlineplot)
        self.ids.cdf_graph.add_plot(self.hairlineplot)
        
        self.plotxaxis = LinePlot(color=[0.1, 0.1, 0.1, 1], line_width=1.5)
        self.plotxaxis.points = [(xmin,0), (xmax,0)]
        self.cdfplots.append(self.plotxaxis)
        self.ids.cdf_graph.add_plot(self.plotxaxis)

        self.plotyaxis = LinePlot(color=[0.1, 0.1, 0.1, 1], line_width=1.5)
        self.plotyaxis.points = [(xmin,0), (xmin,1)]
        self.cdfplots.append(self.plotyaxis)
        self.ids.cdf_graph.add_plot(self.plotyaxis)


    def reset_graphs(self):
        '''
        Resetting graphs and sample output.
        Called from new_distribution, new_parameter.
        '''
        self.pxfunction = []
        self.cdfunction = []
        self.clear_graphs()
        self.rv = None
        self.xval1 = self.xval2 = self.cdfval = None
        self.ids.sample_output.text = ''


    def clear_graphs(self):
        '''
        Erase graphs on page 2 and 3 (pdf/pmf and cdf).
        Called from reset_graphs, SetrvValues
        '''
        # erase pdf/pmf graph (page 2)
        for plot in self.pxfplots:
            self.ids.pxf_graph.remove_plot(plot)
        self.pxfplots = []
        self.plotleft = []
        self.plotright = []
        self.plotcentre = []
        self.plotx = []
        self.plotxaxis = []
        self.plotyaxis = []
        self.pxfplots = []
        self.ids.x1_pxf.pos_hint = {'center_x': 0.5, 'top': 0.2}
        self.ids.probleft.pos_hint = {'center_x': 0.3, 'top': 0.3}
        self.ids.probright.pos_hint = {'center_x': 0.7, 'top': 0.3}
        self.ids.x1_cdf.pos_hint = {'center_x': 0.5, 'top': 0.2}

        # erase cdf graph (page 3)
        for plot in self.cdfplots:
            self.ids.cdf_graph.remove_plot(plot)
        self.hairlineplot = []
        self.cdfplots = []
        self.ids.cdf.pos_hint = {'left': 0.0, 'center_y': 0.5}


    def new_edit_value(self, value, id):
        '''
        New x-value, cdf-value or probleft or probright value.
        Called from findparametervalue, new_text_value, new_slider_values
        '''
        if self.rv is None:                     
            if id[0] == 'x':                 # input from an x-value input field
                self.xval1 = self.xval2 = value
            else:                      # input from some probability input field
                if 0.0 < value < 1.0:
                    if id == 'probleft':
                        self.ids.probleft.text = self.show(value, self.ndecp)
                        if self.iscontinuous:
                            self.cdfval = value
                            self.ids.cdf.text = self.show(value, self.ndecp)
                            self.ids.probright.text = self.show(1 - value, self.ndecp)
                    elif id == 'cdf':
                        self.cdfval = value
                        if self.iscontinuous:
                            self.ids.probleft.text = self.show(value, self.ndecp)
                        self.ids.cdf.text = self.show(value, self.ndecp)
                        self.ids.probright.text = self.show(1 - value, self.ndecp)
                    elif id == 'probright':
                        self.cdfval = 1 - value
                        if self.iscontinuous:
                            self.ids.probleft.text = self.show(1 - value, self.ndecp)
                        self.ids.cdf.text = self.show(1 - value, self.ndecp)
                        self.ids.probright.text = self.show(value, self.ndecp)
                    self.Px = 0
                elif not self.ids[id].focus:
                    self.warning(text='Make sure that 0 < value < 1', id=id)
                    return            
                    
            self.findparametervalue()
        else:                  # all parameter values known
            if id[:2] == 'x1':
                self.xval1 = value
                self.xval2 = max(self.xval1, self.xval2)
            elif id[:2] == 'x2':
                self.xval2 = value
                self.xval1 = min(self.xval1, self.xval2)
            else:
                if 0.0 < value < 1.0:
                    if id in {'probleft', 'cdf'}:
                        self.xval1 = self.rv.quantile(value)
                        self.xval2 = max(self.xval1, self.xval2)
                    else:  # id == 'probright':
                        self.xval2 = self.rv.quantile(1 - value)
                        self.xval1 = min(self.xval1, self.xval2)
                elif not self.ids[id].focus:
                    self.warning(text='Make sure that 0 < value < 1', id=id)
                    return            
                        
            self.cdfval = self.rv.cdf(self.xval1)
            self.Px = self.rv.pmf(self.xval1) \
                if self.isdiscrete and self.rv.in_support(self.xval1) else 0
            self.ids.id_slider_pxf.value2 = self.xval2
            self.ids.id_slider_pxf.value = self.xval1
            self.ids.id_slider_cdf.value = self.xval1
            self.draw_pxf()
            self.draw_cdf()
            self.reposition_edits()


    def SetrvValues(self):  
        '''
        Compute xval, cdfval, ndecx, xmin, xmax for a new rv.
        Called from findparametervalue, new_parameter.
        '''       
        self.dat = d.distributions[self.dat.distname[0]](self.dat.parvalues)
        self.rv = self.dat.rv
        self.update_note()
        self.clear_graphs()
        self.xval1 = self.xval2 = self.rv.median() \
                     if isinf(self.rv.mean()) \
                     else self.rv.mean()
        self.cdfval = self.rv.cdf(self.xval1)
        self.Px = self.rv.pmf(self.xval1) \
                  if self.isdiscrete and self.rv.in_support(self.xval1) \
                  else 0
        dispersion = self.rv.quantile(0.75) - self.rv.quantile(0.25) \
                     if isinf(self.rv.var()) \
                     else self.rv.std()
        self.ndecx = 0 if dispersion == 0 \
                       else max(0, int(floor(-log10(dispersion * 0.0009))))
        [xmin, xmax] = [leftsupport, rightsupport] = self.rv.support
        lsupportinf, rsupportinf = isinf(leftsupport), isinf(rightsupport)
        discrete_wide = self.isdiscrete and rightsupport - leftsupport > 30
        continuous_finite = self.iscontinuous and not(lsupportinf or rsupportinf)
        if self.iscontinuous or discrete_wide:
            if rsupportinf:
                xmax = self.rv.quantile(0.9995)
            if lsupportinf:
                xmin = self.rv.quantile(0.0005)
            if continuous_finite or not lsupportinf:
                xmin = xmin - 0.1 * (xmax - xmin)
            if continuous_finite or not rsupportinf:
                xmax = xmax + 0.1 * (xmax - xmin)
        if self.isdiscrete:
            xmin = int(
                max(floor(xmin - 0.1 * (xmax - xmin)),
                    leftsupport - 1))
            xmax = int(
                min(ceil(xmax + 0.1 * (xmax - xmin)),
                    rightsupport + 1))
            for x in range(xmin + 1, xmax):
                self.pxfunction.append((x, self.rv.pmf(x)))
            for x in range(xmin, xmax):
                self.cdfunction.append((x, self.rv.cdf(x)))
        else:  # iscontinuous
            x_ = lambda i: xmin + i / 600.0 * (xmax - xmin)
            for i in range(601):
                self.pxfunction.append((x_(i), self.rv.pdf(x_(i))))
                self.cdfunction.append((x_(i), self.rv.cdf(x_(i))))
        self.xmin, self.xmax = xmin, xmax
        self.draw_pxf()
        self.draw_cdf()
        self.reposition_edits()


    def findparametervalue(self):
        '''
        Find new parameter value if all other parameter values and x and cdf(x) 
        are known.
        Called from new_parameter, new_edit_value
        '''

        def equal_signs(x,y):      # this function avoids importing numpy(.sign)
            return copysign(1,x)==copysign(1,y)

        eps = 10 * d.eps
        dat = self.dat
        i = dat.unknown_parameters()[0]           #   the parameter to be fitted
        if len(dat.unknown_parameters()) == 1 \
            and  \
                not ('integer' in dat.parspecs[i] # parameter not integer valued 
                     or self.xval1 is None        #      x-value should be known
                     or self.cdfval is None):     #       cdf(x) should be known
            y = self.cdfval                       #          target cdf(x)-value

            def F(c):
                '''
                    for a possible parameter value c: return the difference
                    between target y = cdf(x) and actual cdf_c(x)
                '''
                dat.parvalues[i] = c   # if the unknown parameter value is c ...
                Fdat = d.distributions[dat.distname[0]](dat.parvalues)   # a new
                Frv = Fdat.rv       # Fdat and corresponding Frv are initialized
                return y - Frv.cdf(self.xval1)
            
            # In the next 15 lines two parameter values, lo and hi,
            # are to be found such that the interval [lo, hi] will contain 
            # the unknown parameter value, i.e.: y - F_lo(x) and y - F_hi(x)
            # have different signs. If no such lo and hi are found,  
            # a popup message is displayed.
            # If suitable lo and hi are found, the brentq routine is executed.
                
            if 'lo' in dat.parspecs[i] and 'hi' in dat.parspecs[i]:
                lo, hi = dat.parspecs[i]['lo'] + eps, dat.parspecs[i]['hi'] - eps
            elif 'lo' in dat.parspecs[i]:         # no upper bound for parameter
                lo, hi = dat.parspecs[i]['lo'] + eps, dat.parspecs[i]['lo'] + 10
                while (equal_signs(F(lo), F(hi)) and hi < 1e+10):
                    hi = hi + 10 * (hi - lo)
            elif 'hi' in dat.parspec[i]:          # no lower bound for parameter              
                lo,hi = dat.parspecs[i]['hi'] - 10, dat.parspecs[i]['hi'] - eps
                while  (equal_signs(F(lo), F(hi)) and lo > -1e+10):
                    lo = lo - 10 *(hi - lo)
            else:                    # no upper and no lower bound for parameter
                lo, hi = -1, 1 
                while equal_signs(F(lo), F(hi)) and lo > -1e+10 and hi < +1e+10:
                    delta = hi - lo
                    lo, hi = lo - 10 * delta, hi + 10 * delta
                    
            if equal_signs(F(lo), F(hi)):    # finding suitable (lo, hi) failed
                parname = self.dat.parnames[i]
                self.warning('', 'No value found for ' + parname, None)
            else:
                c = d.brentq(F, lo, hi)  # routine in distrib, copied from scipy
                self.dat.parvalues[i] = c
                self.ids['parval' + str(i)].text = str(c)
                
        if self.dat.all_known():                         # no unknown parameters
            xval1 = self.xval1
            cdfval = self.cdfval
            self.SetrvValues()                # keep old xval1 and probabilities
            self.xval1 = self.xval2 = xval1
            self.cdfval = cdfval
            self.new_edit_value(xval1, 'x1_pxf')
            self.switch_to(self.ids.panel_main)


# Methods directly reacting on user input
# =======================================
    '''
    Possible user actions are:                             | handled by/in:
    -------------------------------------------------------------------------
    0. Select one of four possible tabs:                   | on_index and
           specification, pdf/pmf, cdf, sample             | switch_to
                                                             
    1. On the specification page:                            
        a. select a new distribution                       | new_distribution 
        b. specify parameter value(s)                      | new_parameter         
        c. specify reported precision of x-values          | pyqrs.kv, ndecx 
        d. specify reported precision of probabilities     | pyqrs.kv, ndecp
        e. open wikipedia page                             | pyqrs.kv
        f. open a web page with information about PyQRS    | pyqrs.kv
        g. exit the app (on mobile devices)                | pyqrs.kv
        
    2. On the pdf/pmf page:                                  
        a. edit the left probability (red edit field)      | new_text_value
        b. edit the right probability (blue edity field)   | new_text_value
        c. edit an x-value (white edit fields)             | new_text_value
        d. move the slider                                 | new_slider_values
        
    3. On the cdf page:                                                                    
        a. edit the cdf field                              | new_text_value
        b. edit the x-value                                | new_text_value
        c. move the slider                                 | new_slider_values
        
    4. On the sample page:                                   
        a. edit the sample size                            | new_sample
        b. draw a random sample                            | new_sample       
    '''


    def on_index(self, instance, value):
        ''' switch to another tab index '''
        tab = instance.current_slide.tab
        if self.current_tab != tab:
            self.switch_to(tab)


    def switch_to(self, header):
        ''' we have to replace the functionality of the original switch_to '''
        self.current_tab.state = "normal"
        header.state = 'down'
        self._current_tab = header
        # set the carousel to load the appropriate slide
        # saved in the screen attribute of the tab head
        self.carousel.index = header.slide
        self.reposition_edits()


    def new_distribution(self, id):
        '''
        The user selects a new distribution (parameter values still unknown).
        Called from pyqrs.kv.
        '''
        if id != self.dat.distname[0]:    #new distribution differs from current            
            self.reset_graphs()           #       reset graphics on page 2 and 3
            self.dat = d.distributions[id]() #          create distribution data
            self.isdiscrete = self.dat.isdiscrete
            self.iscontinuous = self.dat.iscontinuous
            self.note = self.dat.note
            self.ids.parval0.focus = True #  focus on first parameter edit field


    def new_parameter(self):  
        '''
        The user selects new parameter values.
        Called from __init__ and pyqrs.kv
        '''           
        partext = [self.ids['parval' + str(i)].text 
                   for i in range(self.dat.npars)]
        try:
            self.dat.parvalues = partext
        except ValueError as X:
            i = X.args[0] # the parameter (0, 1 or 2) which has a value error
            hint = self.dat.parhints[i]
            if hint:
                tekst = 'Make sure that '+ hint
            else: tekst = 'No number'
            self.warning('Error in the value of ' + self.dat.parnames[i],
                          tekst, 'parval' + str(i))
            return

        if len(self.dat.unknown_parameters()) == 1 and self.xval1 and self.cdfval:
            self.findparametervalue()
        elif self.dat.all_known(): 
            self.reset_graphs()
            self.SetrvValues()


    def new_text_value(self, text, id):
        '''
        Front-end for new_edit_value if a new string is given.
        Called from pyqrs.kv
        '''
        try:
            value = d.numeval(text)
        except ValueError:
            self.warning(id=id)
            return            
        if value:
            self.new_edit_value(value, id)


    def new_slider_values(self, value, value2):
        '''
        Front-end for new_edit_value to process changes in Slider.
        Called from pyqrs.kv
        '''
        if self.isdiscrete:
            value = round(value)
            value2 = round(value2)
        self.new_edit_value(value, 'x1_slider_pxf')
        self.new_edit_value(value2, 'x2_slider_pxf')


    def new_sample(self, text):
        '''
        Draw a new sample of size (given by the argument text)
        Called from pyqrs.kv.
        '''
        try:
            N = d.numeval(text) # text is of type string and is transformed to float
        except ValueError:
            self.warning(text='Make sure that N > 1')
            return
        if not d.isclosetoint(N) or N<=0:
            self.warning(text='Make sure that N is integer and N > 1')
            return
        if N is not None and self.rv:
            self.Nsample = N = int(N)
            ndecx = 0 if self.isdiscrete else self.ndecx
            self.ids.sample_output.text = ''
            values = self.rv.sample(N)
            for value in values:
                self.ids.sample_output.text += self.show(value, ndecx) + '  '


class PyQRSApp(App):

    def build(self):
        return PyQRSScreen()


if __name__ == '__main__':
   PyQRSApp().run()

#     import PyInstaller.__main__
# 
#     PyInstaller.__main__.run(['main.py', '--noconfirm', '--ascii', 'clean', 'main.spec'])

